import { combineReducers } from 'redux-immutable';
import form from 'containers/Form/reducer';

const reducers = combineReducers({
  form,
});

export default reducers;

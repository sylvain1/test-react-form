import React, { Component } from 'react';
import FormErrors from './FormErrors'

class FormFields extends Component {
  constructor (props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      phone: '',
      errors: {
        name: false,
        email: false,
        phone: false,
      },
      formValid: false,
    };
  };

  handleInputChange = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({ [name]: value }, () => { this.validateField(name, value) });
  };

  validateField = (fieldName, value) => {
    let errors = this.state.errors;

    switch(fieldName) {
      case 'email':
        const emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        errors.email = emailValid ? false : ' is invalid';
        break;
      case 'name':
        const nameValid = value.length >= 2;
        errors.name = nameValid ? false: ' is too short';
        break;
      case 'phone':
        const phoneValid = value.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im);
        errors.phone = phoneValid ? false: ' is invalid';
        break;
      default:
        break;
    }
    this.setState({ errors }, this.validateForm);
  };

  validateForm = () => {
    this.setState({
      formValid: this.state.name &&
                 this.state.email &&
                 this.state.phone &&
                 !this.state.errors.name &&
                 !this.state.errors.email &&
                 !this.state.errors.phone
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    if(this.state.formValid) {
      this.props.setData([{
        name: this.state.name,
        email: this.state.email,
        phone: this.state.phone,
      }]);
      this.resetForm();
    }
  };

  resetForm = () => {
    this.setState({
      name: '',
      email: '',
      phone: '',
      errors: {
        name: false,
        email: false,
        phone: false,
      },
      formValid: false,
    });
  };

  handleResetForm = (event) => {
    event.preventDefault();
    this.resetForm();
  };

  render() {
    return (
      <form action="/" method="post" onSubmit={this.handleSubmit}>

        <div>
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            value={this.state.name}
            onChange={(event) => this.handleInputChange(event)}
            required
          />
        </div>
        <div>
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            name="email"
            value={this.state.email}
            onChange={(event) => this.handleInputChange(event)}
            required
          />
        </div>
        <div>
          <label htmlFor="phone">Phone</label>
          <input
            type="tel"
            id="phone"
            name="phone"
            value={this.state.phone}
            onChange={(event) => this.handleInputChange(event)}
            required
          />
        </div>

        <button
          type="submit"
          disabled={!this.state.formValid}
        >
          Submit
        </button>
        <button
          type="reset"
          onClick={this.handleResetForm}
        >
          Reset
        </button>

        <FormErrors errors={this.state.errors} />

      </form>
    );
  }
  /*onSubmit={this.props.setData}*/
}

export default FormFields;

import React from 'react';
import PropTypes from 'prop-types';

function FormErrors({ errors }) {
  return (
    <div className="formErrors">
      {Object.keys(errors).map((fieldName, i) => {
        if(errors[fieldName]){
          return (
            <p key={i}>{fieldName} {errors[fieldName]}</p>
          )
        } else {
          return null;
        }
      })}
    </div>
  )
}

FormErrors.propTypes = {
  errors: PropTypes.object,
};

export default FormErrors;
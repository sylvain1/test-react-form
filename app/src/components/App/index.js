import React from 'react';
import Form from 'containers/Form';
import UserList from 'containers/UserList';

function App() {
  return (
    <div className="App">
      <Form />
      <UserList />
    </div>
  );
}

export default App;

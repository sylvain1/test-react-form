import React from 'react';
import PropTypes from 'prop-types';

function UserList({ data, removeData }) {

  function handleRemoveData(event) {
    event.preventDefault();
    const id = event.target.dataset.id;

    if(id) {
      removeData(id);
    }
  }

  return (
    <table style={{width:'100%'}}>
      <thead>
        <tr>
          <td>Name</td>
          <td>Email</td>
          <td>Phone</td>
          <td>Remove</td>
        </tr>
      </thead>
      <tbody>
        {data.map((user, id) => {
          return (
            <tr key={id}>
              <td>{user.name}</td>
              <td>{user.email}</td>
              <td>{user.phone}</td>
              <td>
                <button data-id={id} onClick={handleRemoveData}>X</button>
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
}

UserList.propTypes = {
  data: PropTypes.array,
  removeData: PropTypes.func,
};

export default UserList;

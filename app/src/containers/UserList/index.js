import React, { Component } from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import UserListView from 'components/UserList';
import PropTypes from 'prop-types';
import { selectData } from '../Form/selectors';
import { removeData } from '../Form/actions';

class UserList extends Component {
  render() {
    return (
      <UserListView
        data={this.props.data}
        removeData={this.props.removeData}
      />
    );
  }
}

UserList.propTypes = {
  data: PropTypes.array,
};

const mapStateToProps = createSelector(
  selectData(),
  (data) => ({ data }),
);

const mapDispatchToProps = (dispatch) => ({
  dispatch,
  removeData: (id) => dispatch(removeData(id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList);

import { fromJS } from 'immutable';
import {
  SET_DATA,
  REMOVE_DATA,
} from './constants';

const intialState = fromJS({
  data: [],
});

export default function(state = intialState, action) {
  switch (action.type) {
    case SET_DATA: {
      return state.merge({
        data: [...state.get('data').toJS(), ...action.data],
      });
    }
    case REMOVE_DATA: {
      return state.merge({
        data: state.get('data').delete(action.id),
      });
    }
    default: {
      return state;
    }
  }
}

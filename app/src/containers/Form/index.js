import React, { Component } from 'react';
import { connect } from 'react-redux';
import FormFields from 'components/Form/FormFields';
import { setData } from './actions';

class Form extends Component {
  render() {
    return <FormFields setData={this.props.setData} />;
  }
}

const mapDispatchToProps = (dispatch) => ({
  dispatch,
  setData: (data) => dispatch(setData(data)),
});

export default connect(
  null,
  mapDispatchToProps
)(Form);

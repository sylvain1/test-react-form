import { createSelector } from 'reselect';

/**
 * Direct selector to the measurements state
 */
const selectForm = () => (state) => state.get('form');

/**
 * Selector to measurements data
 */
const selectData = () => createSelector(
  selectForm(),
  (substate) => substate.get('data').toJS()
);

export {
  selectData,
};

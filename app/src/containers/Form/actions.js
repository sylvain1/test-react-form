import {
  SET_DATA,
  REMOVE_DATA,
} from './constants';

export function setData(data) {
  return {
    data,
    type: SET_DATA,
  };
}

export function removeData(id) {
  return {
    id,
    type: REMOVE_DATA,
  };
}

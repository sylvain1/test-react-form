import { createStore, applyMiddleware, compose } from 'redux';
import { fromJS } from 'immutable';
import reducers from 'reducers';

export default function configureStore() {
  const middlewares = [
  ];

  const enhancers = [
    applyMiddleware(...middlewares),
  ];

  // If Redux DevTools Extension is installed use it, otherwise use Redux compose
  const composeEnhancers =
    process.env.NODE_ENV !== 'production' &&
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
      window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

  const store = createStore(
    reducers,
    fromJS({}),
    composeEnhancers(...enhancers)
  );

  return store;
}
